# One Shot Planning

These are some markdown formatting you can use to plan. You can preview your markdown in VS Code with the command "markdown preview", commonly `Command + Shift + V` (`Ctrl + Shift + V` on Windows) while you have this file open

Edit this and make it your own. Alternatively, link your notion here:

## My Notion / weblink

https://notion...

## Steps

### Step 1

* [ ] python -m venv ./venv
* [ ] source venv/Scripts/activate
* [ ] pip install django
* [ ] python.exe -m pip install --upgrade pip
* [ ] pip install black
* [ ] pip install flake8
* [ ] pip install djlint
* [ ] python -m pip install django-debug-toolbar
* [ ] pip freeze > requirement.txt
* [ ] push to repo

### Step 2

* [ ] django-admin startproject brain_two
* [ ] in brain_two dir: python manage.py startapp todos
* [ ] python manage.py makemigrations 
* [ ] python manage.py migrate
* [ ] python manage.py createsuperuser
* [ ] python manage.py runserver
* [ ] http://localhost:8000/admin/
### Step 3

* [ ] create TodoList model
* [ ] Make migrations and migrate
* [ ] test using the shell
* [ ] >>> from todos.models import TodoList
* [ ] >>> todolist = TodoList.objects.create(name="Reminders")    
### Step 4 

* [ ] add TodoList model with Admin
* [ ] @admin.register(TodoList)
* [ ] class TodoAdmin(admin.ModelAdmin):
* [ ]   list_display = (
* [ ]        "name",
* [ ]        "id"
    )
### Step 5

* [ ] create Todo Item Model
* [ ] class TodoItem(models.Model):
* [ ]    task = models.CharField(max_length=100)
* [ ]    due_date = models.DateTimeField(
* [ ]        blank=True,
* [ ]        null=True
* [ ]        )
* [ ]    is_completed = models.BooleanField(default=False)
* [ ]    list = models.ForeignKey(
* [ ]        "TodoList",
* [ ]        related_name="items",
* [ ]        on_delete=models.CASCADE
* [ ]    )
### Step 

* [ ] Regsiter TodoItem model with Admin
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 
### Step 

* [ ] 
* [ ] 

## Questions

How do I access the model instances of the reverse relationship of a many to many relationship?

## Resources

* https://ccbv.co.uk/
